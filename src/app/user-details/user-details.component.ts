import { Component, OnInit, Input } from '@angular/core';
import { User } from '../_models';
import { AccountService } from '../_services';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {


  @Input() users:User[];

  constructor(private accountService: AccountService,
               ) { }

ngOnInit() {
    this.getUsers();
          }
     public getUsers(): void {
           this.accountService.getAll().subscribe((response: User[]) => {
            this.users = response;
             })
              }

}
