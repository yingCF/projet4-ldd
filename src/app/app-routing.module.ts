import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { TranslateComponent } from './translate/translate.component';
import { AuthGuard } from './_helpers';
import { UserProfilComponent } from './user-profil/user-profil.component';

const accountModule = () => import('./account/account.module').then(x => x.AccountModule);
const usersModule = () => import('./users/users.module').then(x => x.UsersModule);

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'translate', component: TranslateComponent ,canActivate: [AuthGuard]},
    { path: 'myprofil', component: UserProfilComponent ,canActivate: [AuthGuard]},
    { path: 'users', loadChildren: usersModule,canActivate: [AuthGuard]},
    { path: 'account', loadChildren: accountModule },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }