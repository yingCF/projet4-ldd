export class Word{
    code : string=" ";
    word : string=" ";

    public getCode(): string{
        return this.code;
    }

    public setCode(code:string){
        this.code=code;
    }

    public getWord(): string{
        return this.word;
    }

    public setWord(word:string){
        this.word=word;
    }


}