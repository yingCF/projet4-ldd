import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map , flatMap ,toArray ,filter} from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Word } from '../data/Word';
import { TranslateWord } from '../data/translateWord';

@Injectable({
  providedIn: 'root'
})

export class TraductionService {
 
  private urlTranslateWord = "https://spring-api-mongo-wise-rabbit-wx.cfapps.io/api/langues/getword";  
  constructor(private http: HttpClient) { }


  public getTranslateWord(word : Word ) : Observable<TranslateWord> {

    return this.http.post<TranslateWord>(this.urlTranslateWord,word);
  }

  public getLanguage() : Observable<TranslateWord[]> {
  
    let url ="https://spring-api-mongo-wise-rabbit-wx.cfapps.io/api/translatewords";
    
    return this.http.get<TranslateWord[]>(url);
    //return this.http.get(`${this.baseUrl}/${id}`,httpOptions);
  }

}
