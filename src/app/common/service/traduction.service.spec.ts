import { TestBed, inject } from '@angular/core/testing';

import { TraductionService } from './traduction.service';

describe('TraductionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TraductionService]
    });
  });

  it('should be created', inject([TraductionService], (service: TraductionService) => {
    expect(service).toBeTruthy();
  }));
});
