﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../_models';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

@Injectable({ providedIn: 'root' })
export class AccountService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    
    private loginUrl = 'https://projet4-ldd-reliable-antelope-ek.cfapps.io/api/auth/signin';
    private signupUrl = 'https://projet4-ldd-reliable-antelope-ek.cfapps.io/api/auth/signup';
    private usersListUrl= 'https://projet4-ldd-reliable-antelope-ek.cfapps.io/api/auth/users';
    private userIDUrl= 'https://projet4-ldd-reliable-antelope-ek.cfapps.io/api/auth/users';

    constructor(
        private router: Router,
        private http: HttpClient
    ) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }

    login(username, password) {
        return this.http.post<User>(this.loginUrl, { username, password },httpOptions)
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
                localStorage.setItem('token', httpOptions.headers.get("fake-jwt-token"));
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
        this.userSubject.next(null);
        this.router.navigate(['/account/login']);
    }

    register(user: User) : Observable<string> {
        return this.http.post<string>(this.signupUrl, user,httpOptions);
    }

    getAll() : Observable<User[]> {
        console.log("++++++++++++++++++++++++++++++");
        return this.http.get<User[]>(this.usersListUrl);
    }

    getById(id: number) {
        return this.http.get<User>(`${this.userIDUrl}/${id}`,httpOptions);
    }

    update(id, params) {
        return this.http.put(`${this.userIDUrl}/${id}`, params)
            .pipe(map(x => {
                // update stored user if the logged in user updated their own record
                if (id == this.userValue.id) {
                    // update local storage
                    const user = { ...this.userValue, ...params };
                    localStorage.setItem('user', JSON.stringify(user));

                    // publish updated user to subscribers
                    this.userSubject.next(user);
                }
                return x;
            }));
    }

    delete(id: number) {
        console.log("user has been deleted")
        return this.http.delete(`${this.userIDUrl}/${id}`)
            .pipe(map(x => {
                // auto logout if the logged in user deleted their own record
                if (id == this.userValue.id) {
                    this.logout();
                }
                return x;
            }));
    }
}