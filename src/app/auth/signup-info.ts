export class SignUpInfo {
    id: string;
    name: string;
    username: string;
    email: string;
    role: string[] = ['user'];
    password: string;
    token: string;

    constructor(name: string, username: string, email: string, password: string) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = ['user'];
    }
}
