import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

import { JwtResponse } from './jwt-response';
import { AuthLoginInfo } from './login-info';
import { SignUpInfo } from './signup-info';
import { Router } from '@angular/router';
import { User } from '../_models';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private infoSubject: BehaviorSubject<SignUpInfo>;
  public info : Observable<SignUpInfo>;

  private loginUrl = 'https://projet4-ldd-reliable-antelope-ek.cfapps.io/api/auth/signin';
  private signupUrl = 'https://projet4-ldd-reliable-antelope-ek.cfapps.io/api/auth/signup';
  private userIDUrl= 'https://projet4-ldd-reliable-antelope-ek.cfapps.io/api/auth/users';

  constructor(
    private router: Router,
    private http: HttpClient
) {
    this.infoSubject = new BehaviorSubject<SignUpInfo>(JSON.parse(localStorage.getItem('info')));
    this.info = this.infoSubject.asObservable();
}

  attemptAuth(credentials: AuthLoginInfo): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  signUp(info: SignUpInfo): Observable<string> {
    return this.http.post<string>(this.signupUrl, info, httpOptions);
  }

  getById(id: number) {
    return this.http.get<User>(`${this.userIDUrl}/${id}`,httpOptions);
}



}
