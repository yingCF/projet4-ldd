﻿export class User {
    // id: string;
    // username: string;
    // password: string;
    // firstName: string;
    // lastName: string;
    // token: string;
    id: number;
    name: string;
    username: string;
    email: string;
    password: string;
    role: string[];
    token: string;
  isDeleting: boolean;
    
    constructor(name: string, username: string, email: string, password: string) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = ['user'];
    }

}