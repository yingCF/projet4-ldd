import { Component, OnInit } from '@angular/core';
import { AccountService } from '../_services/account.service';
import { User } from '../_models';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { AlertService } from '../_services';

@Component({
  selector: 'app-list-component',
  templateUrl: './list-component.component.html',
  styleUrls: ['./list-component.component.css']
})
export class ListComponentComponent implements OnInit {

  users: User[];

    constructor(private accountService: AccountService,
                private alertService: AlertService) {}

    ngOnInit() {
      this.getUsers();
    }

    public getUsers(): void {
      this.accountService.getAll().subscribe((response: User[]) => {
        this.users = response;
      })
    }

    deleteUser(id: number) {
        const user = this.users.find(x => x.id === id);
        this.accountService.delete(id)
            .pipe(first())
            .subscribe(() => {
              this.alertService.success('delete successful', { keepAfterRouteChange: false });
                this.users = this.users.filter(x => x.id !== id) 
            });
    }
}