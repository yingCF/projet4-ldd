import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './Users-routingModule';
import { LayoutComponent } from './layout.component';
import { ListComponentComponent } from './list-component.component';
import { AddEditComponent } from './add-edit.component';
import { AdduserComponent } from '../adduser/adduser.component';




@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        UsersRoutingModule
    ],
    declarations: [
        LayoutComponent,
        ListComponentComponent,
        AddEditComponent,
        AdduserComponent
       
    ]
})
export class UsersModule { }