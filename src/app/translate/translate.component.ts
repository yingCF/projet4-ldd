import { Component, OnInit, Query } from '@angular/core';
import { TraductionService } from '../common/service/traduction.service';
import { Word } from '../common/data/Word';
import { TranslateWord } from '../common/data/translateWord';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.css']
})
export class TranslateComponent implements OnInit {

  myimage : string ="assets/images/logo7.png";
  word : Word = new Word();
  source : string;
  target : string;
  codeLangueSource : string;
  codeLangueCible : string;
  translates2 : TranslateWord[];
  translates : Word[];

  constructor(private traductionService:TraductionService){

    traductionService.getLanguage()
    .subscribe(
      (tabLangues)=>{this.gererTabLangues(tabLangues)},
      (error) => {console.log(error);}
     );
  }
  
  gererTabLangues(tabLangues:TranslateWord[]){
    this.translates2=tabLangues; 
    this.codeLangueSource=this.translates2[0].word.code;
    this.codeLangueCible=this.translates2[1].word.code;
 }
  
  getWords(){
    this.word.setCode(this.codeLangueSource);
    this.word.setWord(this.source);
    
   this.traductionService.getTranslateWord(this.word)
                          .subscribe(data=>{
                            this.translates = data.translats;
                            console.log(data)
                          })
 }

  ngOnInit(): void {

  }
  
}
